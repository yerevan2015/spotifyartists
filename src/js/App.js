import React from 'react';
import axios from 'axios';

import SearchForm from './components/SearchForm';
import SearchResults from './components/SearchResults';


export default class App extends React.Component {
    lastSearchTerm: "";
    searchTermChangeTimeout = null;

    constructor() {
        super();

        let self = this;

        this.state = {
            searchTerm: '',
            searchResults: []
        };

        this.handleSearchTermChange = this.handleSearchTermChange.bind(this);
        this.saveArtist = this.saveArtist.bind(this);
    }

    handleSearchTermChange(value) {
        let self = this;

        self.setState({
            searchTerm: value
        });

        if ( self.searchTermChangeTimeout ) {
            clearTimeout(self.searchTermChangeTimeout);
            self.searchTermChangeTimeout = null;
        }

        self.searchTermChangeTimeout = setTimeout(function () {
            self.searchTermChangeTimeout = null;

            if (self.state.searchTerm !== self.lastSearchTerm) {
                self.lastSearchTerm = self.state.searchTerm;

                if (self.state.searchTerm === '') {
                    self.setState({
                        searchResults: []
                    });
                }
                else {
                    self.search();
                }
            }
        }, 250);
    }

    search() {
        let self = this;

        axios.get('http://localhost:8080/artists', {params: {term: encodeURIComponent(self.state.searchTerm)}})
            .then(function (response) {
                if ([200, 304].indexOf(response.status) != -1) {
                    self.setState({
                        searchResults: self.parseArtistsResponse(response.data)
                    });
                }
                else {
                    // TODO: handle fail
                }
            })
            .catch(function (error) {
                // TODO: handle error
            });
    }

    parseArtistsResponse(artistsResponse) {
        let artists = [];

        artistsResponse.artists.items.forEach(function (artistData) {
            let artist = {
                id: artistData.id,
                name: artistData.name,
                imageUrl: artistData.images && artistData.images.length ? artistData.images[0].url : ""
            };

            artists.push(artist);
        });

        return artists;
    }

    saveArtist(artist) {
        let config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            transformRequest: (data, headers) => {
                let str = [];

                for (let p in data) {
                    if (data.hasOwnProperty(p) && data[p]) {
                        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(data[p]));
                    }
                }

                return str.join('&');
            }
        };

        axios.post('http://localhost:8080/artist', artist, config)
            .then(function (response) {
                if ([200, 304].indexOf(response.status) != -1) {
                }
                else {
                    // TODO: handle fail
                }
            })
            .catch(function (error) {
                // TODO: handle error
            });
    }

    render() {
        const searchTerm = this.state.searchTerm;
        const searchResults = this.state.searchResults;

        return (
            <div>
                <header class="page-header"><h1>Spotify Artists</h1></header>

                <div class="page-body">
                    <SearchForm searchTerm={searchTerm} handleSearchTermChange={this.handleSearchTermChange}></SearchForm>

                    <SearchResults searchResults={searchResults} saveArtist={this.saveArtist}></SearchResults>
                </div>
            </div>
        );
    }
}