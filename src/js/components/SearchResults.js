import React from 'react';


export default class SearchResults extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let self = this;

        const searchResults = self.props.searchResults;

        if ( !searchResults.length ) {
            return null;
        }

        let artistRows = [];

        searchResults.forEach(function (artist) {
            let artistRow = (
                <tr key={artist.id}>
                    <td>{artist.name}</td>
                    <td><img src={artist.imageUrl} alt={artist.name + " image"}/></td>
                    <td><a class="save-button" onClick={() => self.props.saveArtist(artist)}>Save</a></td>
                </tr>
            );

            artistRows.push(artistRow);
        });

        return (
            <div class="search-results">
                <table>
                    <thead>
                    <tr>
                        <th>Artist Name</th>
                        <th>Artist Image</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    {artistRows}
                    </tbody>
                </table>
            </div>
        );
    }
}