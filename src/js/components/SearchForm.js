import React from 'react';


export default class SearchForm extends React.Component {
    constructor(props) {
        super(props);

        this.onArtistChange = this.onArtistChange.bind(this);
    }

    onArtistChange(event) {
        this.props.handleSearchTermChange(event.target.value);
    }

    render() {
        const searchTerm = this.props.searchTerm;

        return (
            <div>
                <form class="artist-form" action="#">
                    <input type="text" name="artist" value={searchTerm} onChange={this.onArtistChange} placeholder="Artist Name"/>
                </form>
            </div>
        );
    }
}
