import web
import urllib.request
import pymysql.cursors

urls = (
    '/artists(.*)', 'artists',
    '/artist(.*)', 'artist'
)

app = web.application(urls, globals())

class artists:
    def OPTIONS(self, data):
        web.header('Access-Control-Allow-Origin', '*')
        web.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS')
        web.header('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With')
    def GET(self, data):
        web.header('Access-Control-Allow-Origin', '*')
        web.header('Access-Control-Allow-Credentials', 'true')

        return urllib.request.urlopen("https://api.spotify.com/v1/search?type=artist&q=" + web.input().term).read()

class artist:
    def OPTIONS(self, data):
        web.header('Access-Control-Allow-Origin', '*')
        web.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS')
        web.header('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With')
    def POST(self, data):
        web.header('Access-Control-Allow-Origin', '*')
        web.header('Access-Control-Allow-Credentials', 'true')

        input = web.input()

        # Open database connection
        db = pymysql.connect("localhost","root","admin","testdb")

        # prepare a cursor object using cursor() method
        cursor = db.cursor()

        # Prepare SQL query to INSERT a record into the database.
        sql = 'INSERT INTO `artist` (`name`, `image_url`) VALUES ("' + input.name + '", "' + input.imageUrl + '")'


        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Commit your changes in the database
            db.commit()
        except Exception as e:
            # Rollback in case there is any error
            db.rollback()

        # disconnect from server
        db.close()

        return


# def addArtist( name, imageUrl ):
#     "This adds artist record into database"
#     mylist = [1,2,3,4]; # This would assig new reference in mylist
#     print "Values inside the function: ", mylist
#     return

if __name__ == "__main__":
    app.run()