CREATE DATABASE `testdb`;

CREATE TABLE `testdb`.`artist` (
  `name` VARCHAR(40) NULL,
  `image_url` VARCHAR(200) NULL);